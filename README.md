***Sudoku Solver***

**Submission details**

Complete project is uploaded to drop box as zip file Workday.zip. I have used personal GIT repo which can be found 
here - https://gitlab.com/pradnya-kulkarni/sudokusolver with branch master. 

**Project details :**

1) This program will solve sudoku using two algorithms - BFS (Baseline) and  BFS + forward checking (Compare) algorithms. 
2) This program supports only square sudokus such as 4 X 4, 9 x 9, 16 X 16 etc.

 
**BFS**
-------
 
* In the BFS, I am starting from first node (i.e. index = 0) along with the initial state of a n puzzle grid.
* Move forward till I find first empty node (i.e. node with value 0). 
* For that node, then compute all possible values (1-9) and create candidateList along with puzzle snapshot with each of the 
possible values that satisfy the row, column, box checking. 
* Generic solve() method in class Search iterates over the candidate list and sees if the goal it met. If not,
global metrix is updated with pruneNode() that eliminate all the already occupied node from candidate list.
* If there are multiple possible values for certain node, then I expand() on it. 
* This will continue till goal is met (i.e. index >= N * N).
* Once goal is met, the current nodes, puzzle snapshot is the answer and that value will be saved into solutionLeafNode.


**BFS + Forward checking**
--------------------------

Most of the basic execution stays same as BFS except that the algorithm also does forward checking to understand if the
chosen value is a right choice for empty future cells. To achieve this,  there is a bitmap maintained at each cell. 

The algorithm adds the following steps in addition to baseline BFS:

1) Update bitmap of all possible values on a given row, column, and box based on current cell[i][j]
2) Once a value is chosen at [i][j], it flips a bit at a corresponding location of the value.
3) If bitmap reaches to zero for a particular [x][y] cell, then I know that the value chosen will lead to an impossible solution and the branch
   is terminated early.


Sample Test HW
---------------

Intel Core Duo with 16 GB RAM

Sample Test
-----------

                   { {0, 0, 0, 0, 0, 0, 0, 0, 0},
             
                     {0, 0, 0, 0, 0, 0, 0, 0, 0},
             
                     {1, 9, 8, 3, 4, 2, 5, 6, 7},
             
                     {0, 0, 0, 0, 0, 0, 0, 0, 0},
             
                     {0, 0, 0, 0, 0, 0, 0, 2, 1},
             
                     {7, 1, 3, 9, 2, 4, 8, 5, 6},
             
                     {9, 6, 1, 5, 3, 7, 2, 8, 4},
             
                     {2, 8, 7, 4, 1, 9, 6, 3, 5},
             
                     {3, 4, 5, 2, 8, 6, 1, 7, 9}};


(Baseline) BFS execution time: approx. 41 s
(Compare)  BFS + forward checking time: approx 400 msec

I experimented with many other variations and observed around 10-25x performance improvements with varying degree of cell emptiness. For some, I also observed 50-100x improvement as well.

Note that for very sparsely allocated grid, I got runtime heap exception which is expected due to limited amount of memory on my laptop.

Future improvements
-------------------

1) The compare BFS+forward checking can further be enhanced by picking a node that has least number of possibilities instead of going sequentially from 1 -> N * N
   by implementing a priority queue instead of FIFO queue.

2) After any given state transition, a node with a single possibility (i.e. bitmap with just 1 bit set) can be resolved by putting the number corresponding to the bit
   and re-computing corresponding bit vectors (row, column, and grid).

3) Threading: The given framework can be extended to use given set of WorkerThreads that could independently evaulate given set of values. However, given I only had a 2 core CPU system, I did not have any ways to test any speed ups in more meaningful ways with threading. 


**Project structure**

Based on the given framework, there are 3 different classes are created namely,

* CandidateNodeSet - implements FrontierNodeSet with generic seach node type.
* BFSSearchNode  - implements Search nodes and overrides isGoal and expands methods for that class.
* BFSSearch - extends abstract class Search and override updateGlobalMetrics,pruneNode and foundSolution with algorithm 
specific logic.

Since it's a Maven project, open the pom.xml in IDE. There I have mutiple test cases under SudokuSolverTest class.
These can be be either run in IDE or via command line. This test cases have sample provided puzzles as well as some 
additional puzzles that were used for testing and debugging purposes. 



