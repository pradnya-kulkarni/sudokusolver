package com.workday.solver;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.workday.solver.impl.bfs.BFSSearchNode;
import com.workday.solver.impl.bfs.BFSSearchNode.ALGO_TYPE;
import com.workday.solver.impl.bfs.CellData;
import com.workday.solver.search.SearchNode;
import org.junit.Test;

public class SudokuSolverTest {

    private static int[][] puzzle1 = {

        {5, 3, 0, 0, 7, 0, 0, 0, 0},

        {6, 0, 0, 1, 9, 5, 0, 0, 0},

        {0, 9, 8, 0, 0, 0, 0, 6, 0},

        {8, 0, 0, 0, 6, 0, 0, 0, 3},

        {4, 0, 0, 8, 0, 3, 0, 0, 1},

        {7, 0, 0, 0, 2, 0, 0, 0, 6},

        {0, 6, 0, 0, 0, 0, 2, 8, 0},

        {0, 0, 0, 4, 1, 9, 0, 0, 5},

        {0, 0, 0, 0, 8, 0, 0, 7, 9}};

    private int[][] solution1 = {

        {5, 3, 4, 6, 7, 8, 9, 1, 2},

        {6, 7, 2, 1, 9, 5, 3, 4, 8},

        {1, 9, 8, 3, 4, 2, 5, 6, 7},

        {8, 5, 9, 7, 6, 1, 4, 2, 3},

        {4, 2, 6, 8, 5, 3, 7, 9, 1},

        {7, 1, 3, 9, 2, 4, 8, 5, 6},

        {9, 6, 1, 5, 3, 7, 2, 8, 4},

        {2, 8, 7, 4, 1, 9, 6, 3, 5},

        {3, 4, 5, 2, 8, 6, 1, 7, 9}};

    private static int[][] puzzle2 = {

        {5, 0, 0, 6, 0, 0, 4, 0, 9},

        {7, 0, 0, 0, 0, 3, 6, 0, 1},

        {0, 0, 0, 0, 9, 1, 0, 8, 2},

        {0, 0, 0, 0, 0, 0, 0, 0, 0},

        {2, 5, 0, 1, 8, 0, 0, 0, 3},

        {0, 0, 0, 3, 0, 6, 0, 4, 5},

        {0, 4, 0, 2, 0, 0, 0, 6, 7},

        {9, 0, 3, 0, 0, 0, 0, 0, 0},

        {6, 2, 0, 0, 3, 0, 1, 0, 8}};

    private int[][] solution2 = {

        {5, 8, 1, 6, 7, 2, 4, 3, 9},

        {7, 9, 2, 8, 4, 3, 6, 5, 1},

        {3, 6, 4, 5, 9, 1, 7, 8, 2},

        {4, 3, 8, 9, 5, 7, 2, 1, 6},

        {2, 5, 6, 1, 8, 4, 9, 7, 3},

        {1, 7, 9, 3, 2, 6, 8, 4, 5},

        {8, 4, 5, 2, 1, 9, 3, 6, 7},

        {9, 1, 3, 7, 6, 8, 5, 2, 4},

        {6, 2, 7, 4, 3, 5, 1, 9, 8}};

    // this takes 35.110 sec to solve with different solution
    private int[][] simple_35 = {

        {0, 0, 0, 0, 0, 0, 0, 0, 0},

        {0, 0, 0, 0, 0, 0, 0, 0, 0},

        {1, 9, 8, 3, 4, 2, 5, 6, 7},

        {0, 0, 0, 0, 0, 0, 0, 0, 0},

        {8, 5, 9, 0, 0, 0, 0, 0, 0},

        {7, 1, 3, 9, 2, 4, 8, 5, 6},

        {9, 6, 1, 5, 3, 7, 2, 8, 4},

        {2, 8, 7, 4, 1, 9, 6, 3, 5},

        {3, 4, 5, 2, 8, 6, 1, 7, 9}};


    private int[][] simple_35_solution = {

        {4, 3, 2, 6, 7, 5, 9, 1, 8},

        {5, 7, 6, 1, 9, 8, 3, 4, 2},

        {1, 9, 8, 3, 4, 2, 5, 6, 7},

        {6, 2, 4, 8, 5, 1, 7, 9, 3},

        {8, 5, 9, 7, 6, 3, 4, 2, 1},

        {7, 1, 3, 9, 2, 4, 8, 5, 6},

        {9, 6, 1, 5, 3, 7, 2, 8, 4},

        {2, 8, 7, 4, 1, 9, 6, 3, 5},

        {3, 4, 5, 2, 8, 6, 1, 7, 9}};

    // this takes 52 sec to solve with different solution
    private int[][] simple_52 = {

        {0, 0, 0, 0, 0, 0, 0, 0, 0},

        {0, 0, 0, 0, 0, 0, 0, 0, 0},

        {1, 9, 8, 3, 4, 2, 5, 6, 7},

        {0, 0, 0, 0, 0, 0, 0, 0, 0},

        {0, 0, 0, 0, 0, 0, 0, 2, 1},

        {7, 1, 3, 9, 2, 4, 8, 5, 6},

        {9, 6, 1, 5, 3, 7, 2, 8, 4},

        {2, 8, 7, 4, 1, 9, 6, 3, 5},

        {3, 4, 5, 2, 8, 6, 1, 7, 9}};

    private int[][] simple_52_solution = {

        {4, 3, 2, 6, 7, 5, 9, 1, 8},

        {5, 7, 6, 1, 9, 8, 3, 4, 2},

        {1, 9, 8, 3, 4, 2, 5, 6, 7},

        {6, 2, 4, 8, 5, 1, 7, 9, 3},

        {8, 5, 9, 7, 6, 3, 4, 2, 1},

        {7, 1, 3, 9, 2, 4, 8, 5, 6},

        {9, 6, 1, 5, 3, 7, 2, 8, 4},

        {2, 8, 7, 4, 1, 9, 6, 3, 5},

        {3, 4, 5, 2, 8, 6, 1, 7, 9}};


    private int[][] fourByFour = {

        {2, 0, 1, 3},

        {1, 3, 0, 4},

        {3, 1, 4, 2},

        {4, 2, 3, 1}};

    private int[][] fourByFour_solution = {

        {2, 4, 1, 3},

        {1, 3, 2, 4},

        {3, 1, 4, 2},

        {4, 2, 3, 1}};

    private int[][] fourByFour_negative = {

        {1, 0, 0, 4},

        {0, 0, 0, 0},

        {2, 3, 0, 0},

        {0, 0, 0, 3}};

    private int[][] fourByFour_solution_negative = {

        {1, 2, 3, 4},

        {3, 4, 1, 2},

        {2, 3, 4, 1},

        {4, 1, 2, 3}};


    @Test
    public void testSudokuSolver_simple_35() {

        long start = System.currentTimeMillis();
        SudokuSolver sudokuSolver = new SudokuSolver(simple_35, ALGO_TYPE.BASIC);
        SearchNode node = sudokuSolver.execute();
        System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));

        assertNotNull(node);
        assertTrue(verify(node, simple_35_solution));
    }

    @Test
    public void testSudokuSolver_simple_35_advanced() {

        long start = System.currentTimeMillis();
        SudokuSolver sudokuSolver = new SudokuSolver(simple_35, ALGO_TYPE.ADVANCED);
        SearchNode node = sudokuSolver.execute();
        System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));

        assertNotNull(node);
        assertTrue(verify(node, simple_35_solution));
    }

    @Test
    public void testSudokuSolver_simple_52() {

        long start = System.currentTimeMillis();
        SudokuSolver sudokuSolver = new SudokuSolver(simple_52, ALGO_TYPE.BASIC);
        SearchNode node = sudokuSolver.execute();
        System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));

        assertNotNull(node);
        assertTrue(verify(node, simple_52_solution));
    }

    @Test
    public void testSudokuSolver_simple_52_ADVANCED() {

        long start = System.currentTimeMillis();
        SudokuSolver sudokuSolver = new SudokuSolver(simple_52, ALGO_TYPE.ADVANCED);
        SearchNode node = sudokuSolver.execute();
        System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));

        assertNotNull(node);
        assertTrue(verify(node, simple_52_solution));
    }

    @Test
    public void testSudokuSolver_fourByFour() {

        long start = System.currentTimeMillis();
        SudokuSolver sudokuSolver = new SudokuSolver(fourByFour, ALGO_TYPE.BASIC);
        SearchNode node = sudokuSolver.execute();
        System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));

        assertNotNull(node);
        assertTrue(verify(node, fourByFour_solution));
    }

    @Test
    public void testSudokuSolver_fourByFour_advanced() {

        long start = System.currentTimeMillis();
        SudokuSolver sudokuSolver = new SudokuSolver(fourByFour, ALGO_TYPE.ADVANCED);
        SearchNode node = sudokuSolver.execute();
        System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));

        assertNotNull(node);
        assertTrue(verify(node, fourByFour_solution));
    }

    @Test
    public void testSudokuSolver_fourByFour_advanced_negative() {

        long start = System.currentTimeMillis();
        SudokuSolver sudokuSolver = new SudokuSolver(fourByFour_negative, ALGO_TYPE.ADVANCED);
        SearchNode node = sudokuSolver.execute();
        System.out.println(System.currentTimeMillis() - start);

        assertNotNull(node);
        assertTrue(verify(node, fourByFour_solution_negative));
    }

    @Test
    public void testSudokuSolver_puzzle1_advanced() {

        long start = System.currentTimeMillis();
        SudokuSolver sudokuSolver = new SudokuSolver(puzzle1, ALGO_TYPE.ADVANCED);
        SearchNode node = sudokuSolver.execute();
        System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));

        assertNotNull(node);
        assertTrue(verify(node, solution1));
    }

    @Test
    public void testSudokuSolver_puzzle1_basic() {

        try {
            long start = System.currentTimeMillis();
            SudokuSolver sudokuSolver = new SudokuSolver(puzzle1, ALGO_TYPE.BASIC);
            SearchNode node = sudokuSolver.execute();
            System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));

            assertNotNull(node);
            fail();
        }catch (Exception ex){

        }
    }

    @Test
    public void testSudokuSolver_puzzle2_advanced() {

        long start = System.currentTimeMillis();
        SudokuSolver sudokuSolver = new SudokuSolver(puzzle2, ALGO_TYPE.ADVANCED);
        SearchNode node = sudokuSolver.execute();
        System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));

        assertNotNull(node);
        assertTrue(verify(node, solution2));
    }

    @Test
    public void testSudokuSolver_puzzle2_basic() {

        try {
            long start = System.currentTimeMillis();
            SudokuSolver sudokuSolver = new SudokuSolver(puzzle2, ALGO_TYPE.BASIC);
            SearchNode node = sudokuSolver.execute();
            System.out.println("Total time taken in ms " + (System.currentTimeMillis() - start));
            fail();
        }catch (Exception ex){

        }
    }


    public boolean verify(SearchNode node , int [][] sourcePuzzle){
        if(node instanceof BFSSearchNode){
            CellData[][] solved = ((BFSSearchNode) node).getPuzzle();

            for(int i=0;i<sourcePuzzle.length;i++){
                for(int j=0;j<sourcePuzzle.length;j++){
                    if(sourcePuzzle[i][j] != solved[i][j].getVal()){
                        return false;
                    }
                }
            }
        }

        return true;
    }

}
