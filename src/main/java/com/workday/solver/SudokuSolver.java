package com.workday.solver;

import com.google.common.base.Preconditions;
import com.workday.solver.impl.bfs.BFSSearch;
import com.workday.solver.impl.bfs.BFSSearchNode;
import com.workday.solver.impl.bfs.BFSSearchNode.ALGO_TYPE;
import com.workday.solver.search.Search.SolveStatus;
import com.workday.solver.search.SearchNode;

public class SudokuSolver {

    private int[][] puzzle;
    private ALGO_TYPE algoType;

    public SudokuSolver(int[][] puzzle, ALGO_TYPE type){
        Preconditions.checkNotNull(puzzle, "Input puzzle cannot be null");
        Preconditions.checkNotNull(type, "ALGO_TYPE cannot be null. Select from BASIC and ADVANCED");

        this.puzzle = puzzle;
        this.algoType = type;
    }

    public SearchNode execute() {

        try {
            // start with the 0,0 node.
            SearchNode searchNode = new BFSSearchNode(0, puzzle, algoType);
            BFSSearch BFSSearch = new BFSSearch(searchNode);
            if(BFSSearch.solve().equals(SolveStatus.SOLUTION_FOUND)) {
                return BFSSearch.getSolutionNode();
            }
        }catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
}
