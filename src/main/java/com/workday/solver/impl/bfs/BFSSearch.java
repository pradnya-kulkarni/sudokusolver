package com.workday.solver.impl.bfs;

import com.workday.solver.search.Search;
import com.workday.solver.search.SearchNode;

public class BFSSearch extends Search {

    public BFSSearch(SearchNode root) {
        super();
        super.root = root;
        super.candidateList = new CandidateNodeSet<>();
        super.candidateList.add(root);
    }


    @Override
    public void updateGlobalMetrics(SearchNode searchNode) {

        BFSSearchNode theNode = (BFSSearchNode) searchNode;

        if (theNode.isOccupied()) {
            return;
        }

        for (int num = 1; num <= theNode.getPossibleValues().length; num++) {
           // System.out.println("Trying number "+num);
            if (theNode.isSafe(num)) {
                theNode.setPossibleValues(num);
            }
        }
    }

    @Override
    public boolean pruneNode(SearchNode node) {
        // The possible values will be empty only if the node is already occupied. In that case we need move to the'
        // next node in the row.

        if(((BFSSearchNode) node).isPosibleValuesEmpty()){
            SearchNode temp = ((BFSSearchNode) node).next();
            if (temp != null) {
                candidateList.add(temp);
            }
            return true;
        }

        return false;
    }

    @Override
    public void foundSolution(SearchNode theNode) {
        ((BFSSearchNode) theNode).print();
        super.solutionLeafNode = theNode;
    }
}
