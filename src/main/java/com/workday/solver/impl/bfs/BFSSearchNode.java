package com.workday.solver.impl.bfs;

import com.workday.solver.search.SearchNode;

import java.util.LinkedList;
import java.util.List;

public class BFSSearchNode implements SearchNode {

    // cell number in give matrics
    // e.g for 9 X 9 matric index ranges from 0-80
    private int index;
    //cell current row
    private int row;
    //cell current column
    private int column;
    // array of possible values
    private int [] possibleValues;
    private int gridSize;
    // this will store cell value and bitmap which is used for forward checking
    private CellData [][] puzzle;

    private ALGO_TYPE type;

    // two algo flavors
    public enum ALGO_TYPE {BASIC, ADVANCED};

    public BFSSearchNode(int index, int[][] puzzle, ALGO_TYPE type) {
        this.index = index;
        this.gridSize = puzzle.length * puzzle.length;
        this.puzzle = new CellData[puzzle.length][puzzle[0].length];
        copyIntoBoard(puzzle,this.puzzle);

        this.row = getRowForIndex(index);
        this.column = getColumnForIndex(index);

        this.possibleValues = new int[puzzle.length];
        this.type = type;

        // initialize initial bitmap
        if(this.type.equals(ALGO_TYPE.ADVANCED)){
            computeBitmap();
        }
    }

    public BFSSearchNode(int index, CellData[][] puzzle,ALGO_TYPE type) {
        this.index = index;
        this.gridSize = puzzle.length * puzzle.length;
        this.puzzle = new CellData[puzzle.length][puzzle[0].length];
        copyCellData(puzzle, this.puzzle);

        this.row = getRowForIndex(index);
        this.column = getColumnForIndex(index);

        this.possibleValues = new int[puzzle.length];
        this.type = type;

        // initialize current state of the bitmap
        if(this.type.equals(ALGO_TYPE.ADVANCED)){
            computeBitmap();
        }
    }

    /**
     * This method computer the bitmap for each cell for each state
     */
    private void computeBitmap() {
        int bitmap = 0;
        int val = 1;
        for(int i=1;i<= puzzle.length;i++){
            val = val << 1;
            bitmap = bitmap | val;
        }

        //System.out.println("***" + Integer.toBinaryString(bitmap));

        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle.length; j++) {
                if (puzzle[i][j].isValidValue()) continue;
                puzzle[i][j].setBitmap(bitmap);
                assignBitmap(i, j);

            }
        }
    }

    /**
     * This method update the bitmap via bit manupulations.
     */
    private void assignBitmap(int row, int column) {
        //check all columns
        for(int i=0;i< puzzle.length;i++){
            if (i == column) continue; //ignore self cell
            if (!puzzle[row][i].isValidValue()) continue; //ignore all empty cells

            int location = 1 << puzzle[row][i].getVal();

            int bitmap = puzzle[row][column].getBitmap();

            //check if this bit is set to 0
            if ((bitmap & location) == 0) continue; //skip this cell since the value is 0 already

            bitmap = bitmap ^ location; //reset the corresponding bit
            puzzle[row][column].setBitmap(bitmap);
        }


        //check all rows
       for(int i=0;i< puzzle.length;i++){
            if (i == row) continue; //ignore self cell
            if (!puzzle[i][column].isValidValue()) continue; //ignore all empty cells

            int location = 1 << puzzle[i][column].getVal();

            int bitmap = puzzle[row][column].getBitmap();

           //check if this bit is set to 0
           if ((bitmap & location) == 0) continue; //skip this cell since the value is 0 already

           bitmap = bitmap ^ location; //reset the corresponding bit

            puzzle[row][column].setBitmap(bitmap);
        }

        //check the self box
         int factor = (int) Math.sqrt(puzzle.length);
        int startRow = row - row % factor;
        int startCol = column - column % factor;

        for (int i = 0; i < factor; i++) {
            for (int j = 0; j < factor; j++) {
                int iRow = i + startRow;
                int jCol = j + startCol;
                if (iRow == row && jCol == column) continue; // ignore self cell
                if (!puzzle[iRow][jCol].isValidValue()) continue; //ignore empty cell

                int location = 1 << puzzle[iRow][jCol].getVal();

                int bitmap = puzzle[row][column].getBitmap();

                //check if this bit is set to 0
                if ((bitmap & location) == 0) continue; //skip this cell since the value is 0 already

                bitmap = bitmap ^ location; //reset the corresponding bit

                puzzle[row][column].setBitmap(bitmap);
            }
        }

    }

    public int[] getPossibleValues() {
        return possibleValues;
    }

    /**
     * check if the possible values are empty. This will happen when cell already have value assigned to it.
     * @return
     */
    public boolean isPosibleValuesEmpty() {
        for(int val : getPossibleValues()){
            if(val == 1){
                return false;
            }
        }

        return true;
    }

    public void setPossibleValues(int value) {
        this.possibleValues[value - 1] = 1;
    }

    @Override
    public boolean isGoal() {
        if (index < gridSize) {
            return false;
        }
        // if any zero are remaining then that should be false
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle.length; j++) {
                if (!puzzle[i][j].isValidValue()) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public List<? extends SearchNode> expand() {

        List<BFSSearchNode> list = new LinkedList<>();

        for(int i=0; i < possibleValues.length;i++){
            if(possibleValues[i] == 1) {
                puzzle[row][column].setVal( i + 1);

                list.add(new BFSSearchNode(index + 1, puzzle, this.type));
            }
        }

        return list;
    }

    /**
     * This will move to next index and update the rows and column values
     */
    public SearchNode next() {
        if (index > gridSize) {
            return null;
        }
        while (++this.index < gridSize) {
            this.row = getRowForIndex(index);
            this.column = getColumnForIndex(index);

            if (!puzzle[row][column].isValidValue()) {
                return this;
            }

        }

        //do this to make isGoal work later
        this.index++;
        this.row = getRowForIndex(index);
        this.column = getColumnForIndex(index);

        return this;
    }

    /**
     * Finding the row based on index
     * */
    private int getRowForIndex(int idx) {
        return idx / puzzle.length;
    }

    /**
     * Finding the column based on index
     **/
    private int getColumnForIndex(int idx) {
        return idx % puzzle.length;
    }

    /**
     * This will check if the value is occupied
     */

    public boolean isOccupied() {
        // for the corner case, if the index
        if (index >= gridSize) {
            return true;
        }
        return puzzle[row][column].getVal() != 0;
    }

    /**
     *  Clone the board for each possible value
     **/
    private CellData[][] cloneBoard(int[][] board) {
        CellData[][] newBoard = new CellData[board.length][board[0].length];
        copyIntoBoard(board, newBoard);
        return newBoard;
    }

    protected void copyIntoBoard(int[][] src, CellData[][] dst) {
        for (int i = 0; i < src.length; i++) {
            for(int j=0;j<src[0].length;j++){
                dst[i][j] = new CellData();
                dst[i][j].setVal(src[i][j]);
            }
        }
    }

    protected void copyCellData(CellData[][] src, CellData[][] dst) {
        for (int i = 0; i < src.length; i++) {
            for(int j=0;j<src[0].length;j++){
                dst[i][j] = new CellData();
                dst[i][j].setVal(src[i][j].getVal());
                dst[i][j].setBitmap(src[i][j].getBitmap());
                dst[i][j].setCount(src[i][j].getCount());
            }
        }
    }


    public boolean isBitMapValid(int i, int j, int num) {
        if (puzzle[i][j].isValidValue()) return true;

        if (i == row && j == column) return true; //ignore current cell in consideration

        int location = 1 << num;

        int bitmap = puzzle[i][j].getBitmap(); //get bitmap

        //check if this bit is set to 0, and
        //skip this cell since the value is 0 already
        if ((bitmap & location) == 0) return true;

        //try to reset the bit
        bitmap = bitmap ^ location;

        //if bitmap == 0, this means no valid possibilities left, so return false
        if (bitmap == 0) {
            return false;

        }

        return true;
    }

    /**
     * Check if the give number will be a safe to use at row and column
     */
    public boolean isSafe(int num) {

        int len = puzzle.length;
        // Check if we find the same num in the similar row ,
        // we return false
        for (int x = 0; x < len; x++) {
            if (puzzle[row][x].getVal() == num) {
                return false;
            }

            if(type.equals(ALGO_TYPE.ADVANCED)) {
                if (!isBitMapValid(row, x, num)) {
                    return false;
                }
            }
        }

        // Check if we find the same num in the similar column,
        // we return false
        for (int x = 0; x < len; x++) {
            if (puzzle[x][column].getVal()== num) {
                return false;
            }

            if(type.equals(ALGO_TYPE.ADVANCED)) {
                 if (!isBitMapValid(x, column, num)) {
                    return false;
                }
            }
        }

        int factor = (int) Math.sqrt(puzzle.length);
        // Check if we find the same num in the particular 3*3 we return false
        int startRow = row - row % factor, startCol =
            column - column % factor;
        for (int i = 0; i < factor; i++) {
            for (int j = 0; j < factor; j++) {
                if (puzzle[i + startRow]
                        [j + startCol].getVal() == num) {
                    return false;
                }
                if(type.equals(ALGO_TYPE.ADVANCED)) {
                    if (!isBitMapValid(i + startRow, j + startCol, num)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     *  A utility function to print grid
     **/
    public void print() {
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle.length; j++) {
                System.out.print(puzzle[i][j].getVal() + " ");
            }
            System.out.println();
        }
    }

    public CellData[][] getPuzzle(){
        return puzzle;
    }
}
