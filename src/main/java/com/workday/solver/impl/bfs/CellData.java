package com.workday.solver.impl.bfs;

public class CellData {
    int val;
    int bitmap;
    int count;

    public CellData() {
        val = 0;
        bitmap = 0;
        count = 0;
    }

    public CellData(int val, int bitmap, int count) {
        this.val = val;
        this.bitmap = bitmap;
        this.count = count;
    }

    public boolean isValidValue(){
        return val != 0;

    }

    public boolean isValPossible(int val){
        int location = 1 << val;
        return (bitmap & location) == 1;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public int getBitmap() {
        return bitmap;
    }

    public void setBitmap(int bitmap) {
        this.bitmap = bitmap;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
