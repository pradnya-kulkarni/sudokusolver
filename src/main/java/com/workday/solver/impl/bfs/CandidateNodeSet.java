package com.workday.solver.impl.bfs;

import com.workday.solver.search.FrontierNodeSet;
import com.workday.solver.search.SearchNode;

import java.util.LinkedList;
import java.util.List;

public class CandidateNodeSet<E extends SearchNode> implements FrontierNodeSet {

    List<E> candidateList = new LinkedList<>();

    public boolean isEmpty() {
        return candidateList.isEmpty();
    }

    public void add(SearchNode aNode) {
        candidateList.add((E) aNode);
    }

    public void add(List<? extends SearchNode> nodeList) {
        nodeList.stream().forEach(node -> add(node));
    }

    public SearchNode removeNextOne() {
        //always remove first
        return candidateList.remove(0);
    }
}
